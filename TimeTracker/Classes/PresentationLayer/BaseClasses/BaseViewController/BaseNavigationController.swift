//
//  BaseNavigationController.swift
//  Inventory+
//
//  Created by Kirill Kunst on 26/08/16.
//  Copyright © 2016 MintRocket LLC Limited. All rights reserved.
//

import UIKit

open class BaseNavigationController: UINavigationController {

    override open func viewDidLoad() {
        super.viewDidLoad()
    }

    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

