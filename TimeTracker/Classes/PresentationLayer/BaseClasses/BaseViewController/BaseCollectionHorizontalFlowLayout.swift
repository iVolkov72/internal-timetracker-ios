//
//  BaseCollectionHorizontalFlowLayout.swift
//  Tewtor
//
//  Created by Kirill Kunst on 05/04/2017.
//  Copyright © 2017 Tewtor Limited. All rights reserved.
//

import UIKit

@IBDesignable class BaseCollectionAutoHeightFlowLayout: UICollectionViewFlowLayout {

    static let estimatedWidth: CGFloat = 100.0
    static let estimatedHeight: CGFloat = 60.0

    override init() {
        super.init()
        self.initialize()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize()
    }

    private func initialize() {
        self.estimatedItemSize = CGSize(
            width: BaseCollectionAutoHeightFlowLayout.estimatedWidth,
            height: BaseCollectionAutoHeightFlowLayout.estimatedHeight
        )
    }

}
