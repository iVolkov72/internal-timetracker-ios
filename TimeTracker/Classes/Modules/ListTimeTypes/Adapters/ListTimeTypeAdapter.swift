//
//  ListTimeTypeAdapter.swift
//  TimeTracker
//
//  Created by Mint Rocket on 20.10.2017.
//  Copyright © 2017 Mint Rocket. All rights reserved.
//

import Foundation
import IGListKit

protocol ListTimeTypeAdapterDelegate: class {
    func getItem(object: TimeTypeItem)
}

public class ListTimeTypeAdapter: ListSectionController {
    
    private var object: TimeTypeItem?
    weak var lttDelegate: ListTimeTypeAdapterDelegate?
    
    override public init() {
        super.init()
        inset = UIEdgeInsets(top: 2, left: 6, bottom: 2, right: inset.left/2 - 2)
    }
    
    override public func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width/2 - 8, height: collectionContext!.containerSize.width/2 - 18)
    }
    
    public override func cellForItem(at index: Int) -> UICollectionViewCell {
        let timeTypeCell = collectionContext!.dequeueReusableCell(withNibName: TimeTypeCell.className(),
                                                                   bundle: nil,
                                                                   for: self,
                                                                   at: index) as? TimeTypeCell
        if let object = self.object {
            timeTypeCell?.configure(typeItem: object)
        }
        return timeTypeCell!
    }
    
    override public func didUpdate(to object: Any) {
        self.object = object as? TimeTypeItem
    }

    public override func didSelectItem(at index: Int) {
        //print("\(String(describing: object?.typeName))")
        
        if let object = object {
            self.lttDelegate?.getItem(object: object)
        }
    }
}

extension NSObject: ListDiffable {
    public func diffIdentifier() -> NSObjectProtocol {
        return self
    }
    
    public func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        return isEqual(object)
    }
}

extension UICollectionViewCell {
    static func className() -> String {
        return String(describing: self)
    }
}

