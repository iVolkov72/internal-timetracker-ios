//
//  MRMRListTimeTypesView.swift
//  TimeTracker
//
//  Created by Puls7289 on 19/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit
import IGListKit

// MARK: - View Controller
final class ListTimeTypesViewController: BaseCollectionViewController, ListTimeTypeAdapterDelegate {
    
    func getItem(object: TimeTypeItem) {
        handler.updateTimeType(item: object)
        print("\(object.typeName)")
    }
    
    var handler: ListTimeTypesEventHandler!
    typealias RouterType = ListTimeTypesRouter
    var router: RouterType!

    @IBOutlet weak var itemCollectionView: UICollectionView!
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        handler.loadViews()
    }
    
    override func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        let adapter = ListTimeTypeAdapter()
        adapter.lttDelegate = self
        return adapter
    }
}

extension ListTimeTypesViewController: ListTimeTypesViewBehavior {
    
    func set(items: [TimeTypeItem]) {
        self.items = items
        self.update()
    }
}
