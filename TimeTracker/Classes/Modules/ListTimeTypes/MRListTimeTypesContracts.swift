//
//  MRListTimeTypesContracts.swift
//  TimeTracker
//
//  Created by Puls7289 on 19/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit

// MARK: - Contracts
protocol ListTimeTypesViewBehavior: class {
    func set(items: [TimeTypeItem])
}

protocol ListTimeTypesEventHandler: class {
    func updateTimeType(item: TimeTypeItem)
    func loadViews()
}
