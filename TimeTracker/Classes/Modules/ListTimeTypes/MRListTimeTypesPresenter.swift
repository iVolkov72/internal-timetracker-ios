//
//  MRListTimeTypesPresenter.swift
//  TimeTracker
//
//  Created by Puls7289 on 19/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit

// MARK: - Presenter
final class ListTimeTypesPresenter {

    weak var view: ListTimeTypesViewBehavior!
    let itemStore = TimeTypeItemStore()
    let calcTimeService = CalcTime()
}

extension ListTimeTypesPresenter: ListTimeTypesEventHandler {
    func updateTimeType(item: TimeTypeItem) {
        self.calcTimeService.startCalcTime(item: item)
    }
    
    func loadViews() {
        view.set(items: itemStore.getItems())
    }
}
