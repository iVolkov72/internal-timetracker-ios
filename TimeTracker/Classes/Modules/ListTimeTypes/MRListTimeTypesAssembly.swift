//
//  MRListTimeTypesAssembly.swift
//  TimeTracker
//
//  Created by Puls7289 on 19/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit

final class ListTimeTypesAssembly {
    class func createModule() -> ListTimeTypesViewController {
        let vc = StoryboardScene.Main.listTimeTypesViewController.instantiate()
        let presenter = ListTimeTypesPresenter()
        let router = ListTimeTypesRouter()

        presenter.view = vc
        vc.handler = presenter
        vc.router = router

        router.viewController = vc

        return vc
    }
}
