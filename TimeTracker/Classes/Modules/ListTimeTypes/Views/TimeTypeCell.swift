//
//  TimeTypeCell.swift
//  TimeTracker
//
//  Created by Mint Rocket on 20.10.2017.
//  Copyright © 2017 Mint Rocket. All rights reserved.
//

import UIKit
import Foundation

class TimeTypeCell: UICollectionViewCell {
    
    var item: TimeTypeItem?
    @IBOutlet weak var timeTypeLabel: UILabel!
    let itemsStore = TimeTypeItemStore()
    
    func configure(typeItem: TimeTypeItem) {
        self.timeTypeLabel.text = typeItem.typeName
        self.backgroundColor = typeItem.typeColor
        self.item = typeItem
    }
}
