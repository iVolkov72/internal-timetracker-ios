//
//  MRMainContainerContracts.swift
//  TimeTracker
//
//  Created by Puls7289 on 19/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit

// MARK: - Contracts
protocol MainContainerViewBehavior: class {

}

protocol MainContainerEventHandler: class {

}