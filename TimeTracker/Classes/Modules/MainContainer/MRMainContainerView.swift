//
//  MRMRMainContainerView.swift
//  TimeTracker
//
//  Created by Puls7289 on 19/10/2017.
//  Copyright © 2017 MintRocket LLC. All rights reserved.
//

import UIKit

// MARK: - View Controller
final class MainContainerViewController: BaseViewController {
    
    var handler: MainContainerEventHandler!
    typealias RouterType = MainContainerRouter
    var router: RouterType!

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

}

extension MainContainerViewController: MainContainerViewBehavior {

}
