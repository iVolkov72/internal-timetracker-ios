//
//  TimeTypeItem.swift
//  TimeTracker
//
//  Created by Mint Rocket on 20.10.2017.
//  Copyright © 2017 Mint Rocket. All rights reserved.
//

import UIKit
import Foundation

class TimeTypeItem: NSObject {
    var typeName: String
    var typeColor: UIColor
    var totalTime: Int
    
    init(name: String, color: UIColor) {
        self.typeName = name
        self.typeColor = color
        self.totalTime = 0
    }
}
