//
//  SwiftyBeaverLogger.swift
//  TimeTracker
//
//  Created by Mint Rocket on 19.10.2017.
//  Copyright © 2017 Mint Rocket. All rights reserved.
//

import UIKit
import SwiftyBeaver

class SwiftyBeaverLogger: LoggerType {
    
    fileprivate let loggerInstance = SwiftyBeaver.self
    
    init() {
        let console = ConsoleDestination()
        let file = FileDestination()
        loggerInstance.addDestination(console)
        loggerInstance.addDestination(file)
    }
    
    func log(_ level: LogLevel, tag: LogTag, className: String, _ message: String) {
        switch level {
        case .debug:
            loggerInstance
                .debug("------- \n [\(tag.rawValue)][\(className)] \n -> \(message) \n")
            break
        case .error:
            loggerInstance
                .error("------- \n [\(tag.rawValue)][\(className)] \n -> \(message) \n")
            break
        case .info:
            loggerInstance
                .info("------- \n [\(tag.rawValue)][\(className)] \n -> \(message) \n")
            break
        case .warning:
            loggerInstance
                .warning("------- \n [\(tag.rawValue)][\(className)] \n -> \(message) \n)")
            break
        default:
            loggerInstance
                .verbose("------- \n [\(tag.rawValue)][\(className)] \n -> \(message) \n")
            break
        }
    }
}
