//
//  CalcTime.swift
//  TimeTracker
//
//  Created by Mint Rocket on 23.10.2017.
//  Copyright © 2017 Mint Rocket. All rights reserved.
//

import Foundation
import UIKit

class CalcTime {
    
    var flagOnOff = false
    var timer = Timer()
    var seconds = 0
    var lastItem: TimeTypeItem = TimeTypeItem(name: "default", color: UIColor.clear)
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    func startCalcTime(item: TimeTypeItem) {
        
        if !lastItem.isEqual(item) {
            self.stop(timer: timer)
            lastItem = item
            flagOnOff = true
            
            registerBackgroundTask()
            timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (time) in
                self.seconds += 1
                print("Start timer on object \(item.typeName): \(self.seconds) seconds")
            })
        } else if flagOnOff {
            endBackgroundTask()
            lastItem = TimeTypeItem.init(name: "default", color: UIColor.clear)
            self.stop(timer: timer)
        }
    }
    
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTask() {
        print("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
    }
    
    func stop(timer: Timer) {
        timer.invalidate()
        print("Stop timer on object total \(self.seconds) seconds")
        self.seconds = 0
        flagOnOff = false
    }
}
