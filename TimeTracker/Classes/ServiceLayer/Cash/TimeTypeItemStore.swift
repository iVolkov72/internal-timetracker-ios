//
//  TimeTypeItemStore.swift
//  TimeTracker
//
//  Created by Mint Rocket on 23.10.2017.
//  Copyright © 2017 Mint Rocket. All rights reserved.
//

import Foundation
import UIKit

class TimeTypeItemStore {
    
    func getItems() -> [TimeTypeItem] {
        let itemsArray: [TimeTypeItem] = [
            TimeTypeItem(name: "Дом", color: .green),
            TimeTypeItem(name: "Перемещение", color: .yellow),
            TimeTypeItem(name: "Отдых", color: .orange),
            TimeTypeItem(name: "Работа", color: .blue),
            TimeTypeItem(name: "Еда", color: .red),
            TimeTypeItem(name: "Учеба", color: .purple)
        ]
        return itemsArray
    }
}
